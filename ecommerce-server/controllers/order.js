const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");

module.exports.addProduct = async (userData,product) => {
	if(userData.isAdmin == false) {
		let userId = await User.findOne({_id: userData.id}).then(result => {
			console.log(result.id);
			return result.id;
		})
		let userName = await User.findOne({_id: userData.id}).then(result => {
		console.log(result.name);
		return result.name;
		})
		let productId = await Product.findOne({_id: product.id}).then(result => {
			console.log(result.id);
			return result.id;
		})
		let productPrice = await Product.findOne({_id: product.id}).then(result => {
			console.log(result.price);
			return result.price;
		})
		let productName = await Product.findOne({_id: product.id}).then(result => {
		console.log(result.name);
		return result.name;
		})
		let productImage = await Product.findOne({_id: product.id}).then(result => {
		console.log(result.image);
		return result.image;
		})
		// function stockUpdate(){Product.findById(productId).then(result => {
		// result.stock = result.stock-1;
		// result.save();
		// }) }

		return await Order.findOne({customerId: userId}).then(result => {
			if (result == null){
				let newOrder = new Order({
					customerId : userId,
					customerName : userName,
					orderedProducts : ({productId:productId,productName:productName, productPrice:productPrice,image:productImage,quantity:1}),
					totalAmount: productPrice
				})
				return newOrder.save().then((user,error) => {
					if (error){
						return false;
					}
					else {
						// stockUpdate();
						return true
					}
				})
			}
			else{
				return Order.findOne({customerId: userId}).then(order => {
					order.orderedProducts.push({productId:productId,productName:productName,image:productImage,productPrice:productPrice,quantity:1});
					order.totalAmount = order.orderedProducts.reduce((a, b) => a + (b.productPrice * b.quantity), 0);
					order.save();
					// stockUpdate();
					return true
				})
			}
		})
	}
	else{
		return false
	}
}

module.exports.addOneProduct = async (userData,product) => {

	let userId = await User.findOne({_id: userData.id}).then(result => {
		console.log(result.id);
		return result.id;
	})

	return await Order.findOne({customerId: userId}).then(result => {
		if (result == null){
			return false
		}
		else{
			return result.orderedProducts.findOne({_id:product.id}).then(data =>{
				data.quantity = data.quantity + 1;
				data.save();
			})
		}
	})


}


module.exports.getAllOrders = async (userData) => {
	if(userData.isAdmin) {
		return Order.find({}).then(result => {
			return result;
		})
	}
	else{
		return false
	}
}

module.exports.getMyOrders = async (userData) => {
	if(userData.isAdmin == false) {
		return Order.findOne({customerId:userData.id}).then(result => {
			if (result == null){
				return false	
			}
			else{
				return result;
			}
		})
	}
	else{
		return false
	}
}

